	.file	"01_print_ascii.cpp"
	.intel_syntax noprefix
	.text
	.section	.rodata
.LC0:
	.string	"%i, %c\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	push	rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	mov	rbp, rsp
	.cfi_def_cfa_register 6
	sub	rsp, 16
	mov	BYTE PTR -1[rbp], 50
.L3:
	movzx	eax, BYTE PTR -1[rbp]
	test	al, al
	js	.L2
	movzx	edx, BYTE PTR -1[rbp]
	movzx	eax, BYTE PTR -1[rbp]
	mov	esi, eax
	lea	rdi, .LC0[rip]
	mov	eax, 0
	call	printf@PLT
	movzx	eax, BYTE PTR -1[rbp]
	add	eax, 1
	mov	BYTE PTR -1[rbp], al
	jmp	.L3
.L2:
	mov	eax, 0
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0"
	.section	.note.GNU-stack,"",@progbits
