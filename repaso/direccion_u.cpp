#include <stdio.h>
#include <stdlib.h>

/*
 *array enteros y apuntar en qué índice del texto aparecen todas las u
 */

int main (int argc, char *argv[]) {

    char frase[] = "Yo lo sabía. Cuando tienen tiempo suelen comer juntos, con el staff o solos. Lo hacen a menudo afirmó el catalán al ser preguntado por el sonado evento";

    char *busca = frase;

    /* Método puntero */

    for (int i=0; )
        while (*busca != 'u' && *busca != '\0')
        busca++;


    if (*busca == 'u') {
        printf (
                "La u se encuentra a %lu posiciones del inicio.\n"
                "En la dirección: %p\n", (busca - frase) / sizeof (char), busca);
    }


    return EXIT_SUCCESS;
}

