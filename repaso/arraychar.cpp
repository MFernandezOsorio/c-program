#include <stdio.h>
#include <stdlib.h>

/* declarar array dentro del main que tenga 20 unsigned char
 *
 */

#define MAX 20

int main (int argc, char *argv[]) {

    unsigned char l[MAX];

    /* Cáclulos */
    for (int i = 0; i < MAX; i++)
        l[i] = (i + 1)*(i + 1);


    /* Salida de Datos */
    printf ("\n| ");
    for (int i = 0; i < MAX; i++)
        printf ("%i | ", l[i]);

    printf ("\n\n");

    return EXIT_SUCCESS;
}
