#include <stdio.h>
#include <stdlib.h>

/*  declarar array de caracteres que se llama frase
 *  preguntar al usuario una palabra y la leemos con scanf con ese array de caracteres
 * crear una lista que contenga dos palabras: hola y buenas noches
 * imprimir lo que ocupa lista
 * Calcular o imprimir la cantidad de celdas que tiene lista
 * Sabiendo la cantidad de celdas, imprimir todas las palabras que tenga
*/

const char * lista[] = {
    "Hola",
    "Buenos días",
    "Good morning",
    "Bonjour",
    NULL

};

int main (int argc, char *argv[]) {

    char frase[20];

    printf ("¿Qué palabra quieres? \n");
    scanf (" %s", frase);
    printf ("La palabra que has elegido es: %s \n\n", frase);
    printf ("Tamaño de lista: %lu bytes. \n", sizeof (lista));
    printf ("Celdas de lista: %lu. \n\n", sizeof (lista) / sizeof (char *));

    printf ("Lista contiene los siguientes caracteres en cada celda: \n");
    int tam = (sizeof (lista) - sizeof (NULL)) / sizeof (char*);
    for (int cont = 0; cont < tam; cont++)
        printf (" -%s \n", lista[cont]);

    printf ("\n");

    char const **p = lista;

    printf ("Otra forma de hacerlo:\n");
    while (*p != NULL){
        printf (" -%s\n", *p);
        p++;
    }

    return EXIT_SUCCESS;
}
