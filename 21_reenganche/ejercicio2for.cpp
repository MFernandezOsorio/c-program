#include <stdio.h>
#include <stdlib.h>


/*
 Escribir un bucle que calcule la suma de cada
 tercer entero, comenzando por i = 2 (es decir,
 calcular la suma de 2+5+8+11+...) para todos
 los valores de i menores que 10O. Escribir el
 bucle de tres formas diferentes:while, do-while, for.
 */

int main (int argc, char *argv[]) {

    int resultado, i=2;

    for (i; i<100; i+=3)
        resultado += i;

    printf ("La suma de cada tercer entero es: %i \n", resultado);

    return EXIT_SUCCESS;
}
