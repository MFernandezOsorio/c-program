#include <stdio.h>
#include <stdlib.h>
#include <cstring>

/*
 * Repite el ejercicio anterior. Para si te encuentras un asterisco.
 */

int main (int argc, char *argv[]) {

    int i=0;
    char texto[] = {
        'a',
        'j',
        'o',
        '*',
        'b'
    };

    while ((texto[i] != '\0') && (texto[i] != '*' && (i < strlen(texto)))){
        printf("En ascii es: %3d\n", texto[i]);
        i++;
    };

    return EXIT_SUCCESS;
}
