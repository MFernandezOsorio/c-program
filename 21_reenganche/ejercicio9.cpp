#include <stdio.h>
#include <stdlib.h>
#include <cstring>

/*
 * Escribir un bucle que examine cada carácter en un array
 * de caracteres llamado texto y cuente
 * cuantos de ellos son letras, cuantos números y el total de los mismos.
 */

#define M 4

int main (int argc, char *argv[]) {

    int cletra = 0,
       cnumero = 0,
       total;
    char texto[M] = {
        'a',
        'j',
        'o',
        '5',
    };

    for (int i=0; i<M; i++){
        if (texto[i] >= 65 && texto[i] <= 122){
            cletra ++;
        }else if (texto[i] >= 48 && texto[i] <= 57){
            cnumero ++;
        }
    }

    total = cletra + cnumero;
    printf("Hay %i letra/s.\n", cletra);
    printf("Hay %i numero/s.\n", cnumero);
    printf("En total hay %i caracteres.\n", total);

    return EXIT_SUCCESS;
}
