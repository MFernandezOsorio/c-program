#include <stdio.h>
#include <stdlib.h>
#include <cstring>

/*
 * Repite el ejercicio anterior sin saber cuántos
 * caracteres hay. Usa strlen. for
 */

int main (int argc, char *argv[]) {

    char texto[] = {
        'a',
        'j',
        'o',
    };

    for(int i=0; i<strlen(texto); i++)
        printf("En ascii es: %3d\n", texto[i]);

    return EXIT_SUCCESS;
}
