#include <stdio.h>
#include <stdlib.h>

/*
 * Repetir el problema anterior empezando por el número que
 * quiera el usuario y sumando 100 números, en vez de los menores que 100.
 */

int main (int argc, char *argv[]) {

    int i=0, inicial, resultado;

    printf("¿Por qué número desea empezar a sumar 2?\n");
    scanf(" %i", &inicial);

    for (inicial, i; i<=100; inicial +=3, i++)
        resultado += inicial;

    printf ("La suma de cada tercer entero es: %i \n", resultado);

    return EXIT_SUCCESS;
}
