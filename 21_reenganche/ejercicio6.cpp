#include <stdio.h>
#include <stdlib.h>
#include <cstring>

/*
 * Repite el ejercicio anterior sin saber
 * cuántos caracteres hay. Usa el '\0'. while.
 */

int main (int argc, char *argv[]) {

    int i=0;
    char texto[] = {
        'a',
        'j',
        'o',
    };

    while (texto[i] != '\0'){
        printf("En ascii es: %3d\n", texto[i]);
        i++;
    };

    return EXIT_SUCCESS;
}
