#include <stdio.h>
#include <stdlib.h>


/*
 * Empezando por 2, vete generando números de 3 en 3
 * mientras sean menores que 100. Calcula
 * la suma de aquellos que sean divisibles por 5.
 * En la primera versión usa ?: y en la otra el if.
 */

int main (int argc, char *argv[]) {

    int resultado;

    for (int i=2; i<100; i+=3){
        if (i<100 && i%5==0){
        resultado +=i;
        }
    }

    printf ("La suma de cada tercer entero divisible por 5 es: %i \n", resultado);

    return EXIT_SUCCESS;
}
