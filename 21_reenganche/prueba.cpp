#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

/*
 * Repetir el problema anterior empezando por el número que
 * quiera el usuario y sumando 100 números, en vez de los menores que 100.
 */

int main (int argc, char *argv[]) {

    char c, result;

    c = 'M';
    result = tolower(c);
    printf("tolower(%c) = %c\n", c, result);

    c = 'm';
    result = tolower(c);
    printf("tolower(%c) = %c\n", c, result);

    c = '+';
    result = tolower(c);
    printf("tolower(%c) = %c\n", c, result);

    return EXIT_SUCCESS;
}
