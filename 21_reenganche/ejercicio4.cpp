#include <stdio.h>
#include <stdlib.h>

/*Escribir un bucle que examine cada carácter en un array de caracteres llamado
 * texto y escriba el equivalente ASCII  (el valor numérico) de cada carácter.
 * Supón que sabes cuántos caracteres hay. for
 */

int main (int argc, char *argv[]) {

    char texto[] = {
        'a',
        'j',
        'o',
    };

    for(int i=0; i<3; i++)
        printf("En ascii es: %3d\n", texto[i]);

    return EXIT_SUCCESS;
}
