if(abs(x)<xmin)x=(x>0)?xmin:-xmin

Descripción/lectura de línea:
- Si un número absoluto es menor que un valor mínimo de un intervalo, se ejecuta el if. Si x es mayor que 0, el valor de x pasa a ser automáticamente en los números que limiten ese intervalo.
Por ejemplo: if (abs(-3)<6)x=(3>0)?6:-6
