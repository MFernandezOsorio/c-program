#include <stdio.h>


int main () {

   printf ("char: %lu bytes.\n", sizeof (char));
   printf ("signed char: %lu bytes.\n", sizeof (signed char));
   printf ("unsigned char: %lu bytes.\n", sizeof (unsigned char));
   printf ("long int: %lu bytes.\n", sizeof (long int));
   printf ("signed long int: %lu bytes.\n", sizeof (signed long int));
   printf ("unsigned long int: %lu bytes.\n", sizeof (unsigned long int));
   printf ("short int: %lu bytes.\n", sizeof (short int));
   printf ("signed short int: %lu bytes.\n", sizeof (signed short int));
   printf ("unsigned short int: %lu bytes.\n", sizeof (unsigned short int));
   printf ("signed int: %lu bytes.\n", sizeof (signed int));
   printf ("unsigned int: %lu bytes.\n", sizeof (unsigned int));
    return 0;
}

