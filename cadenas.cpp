#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXLIN 0x100

const char * const cprog = "cadenas";
char vprog[] = "programa";

void concatena (char *dest, const char *org, int max) {
    /* Avanzar dest hasta el \0
    Copiar caracter a caracter de org a dest como mucho max o hasta \0 */
    while (*dest != '\0') dest++;
    dest++;
   // while (*(dest++) != '\0');
   // for (;*dest != '\0'; dest++);
   // Para meter un mensaje secreto

   for (int c= 0; *org!='\0' && c<max; c++, ++org, ++dest)
       *dest = *org;
   *dest = *org;
}

void cifra (char *frase, int clave){
    while (*(frase) != '\0')
        *(frase++) += clave;
    //for (; *frase != '\0; frase++)
    //    *frase += clave;
}
int main (int argc, char *argv[]) {

    char linea[MAXLIN];
    strcpy (linea, cprog);  // Funcion insegura
    strncpy (linea, cprog, MAXLIN);
    strcat (linea, " ");
    strncat (linea, vprog, MAXLIN - strlen(linea));

    concatena (linea, "Esto es un mensaje secreto", MAXLIN - strlen(linea));

    cifra (linea + strlen(linea) + 1, 3);

    printf ("%s\n", linea);
    printf ("Secreto: %s\n", linea + strlen(linea) + 1);

    cifra (linea + strlen(linea) + 1, -3);
    printf ("Secreto: %s\n", linea + strlen(linea) + 1);

/*1 strcpy copia en linea cprog
 *2 strncp copia en linea cprog y se pone cuantos caracteres quieres copiar
 *3 strcat copia en linea vprog a continuacion del contenido de linea
 *4 Restas a MAXLIN linea para que, si tienes 100 celdas y 20 ocupadas, solo copie 80
 * */

    return EXIT_SUCCESS;
}
