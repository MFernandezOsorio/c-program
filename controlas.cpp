#include <stdio.h>
#include <unistd.h>

#define MAXCOLS 100


int main () {

    /* Las mismas 3 líneas de 3 formas distintas */
    printf (" Hola \x0A \x09 Beep \x07 \x0A");
    printf (" Hola \n \t Beep \a \n");
    printf (" Hola \012 \011 Beep \007 \x012\n");


    printf (" Hola \n \0Esto es secreto ");
    printf ("\n");
    for (int vez=0; vez<=MAXCOLS; vez++) {
        printf ("\r");
        for (int igual=0; igual<vez; igual++)
            printf("=");
        printf ("> %2i%%", vez);
        fflush (stdout);
        usleep (100000);
    }
    printf ("\n.FIN.\n");


    return 0;

}
