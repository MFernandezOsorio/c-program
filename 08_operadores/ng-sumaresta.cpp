#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    char respuesta;
    int num1, num2;
    int resultado;

    printf ("Esto es un programa que suma y resta. \n");

    printf ("(s)uma o (r)esta: ");
    scanf (" %c", &respuesta);

    if (respuesta != 'r' && respuesta != 's' ) {
        fprintf (stderr, "Eres un pringao.\n");
        return EXIT_FAILURE;
    }

     printf ("Por favor, introduzca el primer número: ");
     scanf (" %i", &num1);

     printf ("Ahora, introduzca el segundo número: ");
     scanf (" %i", &num2);

    if (respuesta == 's')
       resultado = num1 + num2;

    if (respuesta == 'r')
        resultado = num1 - num2;

    printf ("%i %c %i = %i\n",
            num1,
            respuesta == 's'? '+' : '-',
            num2,
            resultado);

    return EXIT_SUCCESS;
}
