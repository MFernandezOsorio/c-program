#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    char respuesta;
    int num1, num2;
    int opcion_buena = 0;


    printf ("Esto es un programa que suma y resta. \n");

    printf ("Por favor, introduzca el primer número: ");
    scanf (" %i", &num1);

    printf ("Ahora, introduzca el segundo número: ");
    scanf (" %i", &num2);

    printf ("¿Qué operación desea hacer? Responda s o r: ");
    scanf (" %c", &respuesta);


    if (respuesta == 's') {
        printf ("El resultado es: %i + %i = %i \n", num1, num2, num1 + num2);
        opcion_buena = 1;
    }

    if (respuesta == 'r'){
        printf ("El resultado es: %i - %i = %i \n", num1, num2, num1 - num2);
        opcion_buena = 1;
    }

    if (!opcion_buena)
        printf ("¿Estás tonto o que? Esa operación no existe |:'( \n");

    return EXIT_SUCCESS;
}
