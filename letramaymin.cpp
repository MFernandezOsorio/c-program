/* g++ -DLETRA=\'d\' letramaymin.cpp -o letramaymin */

#include <stdio.h>

#define SALTO ('a' - 'A')

int main () {

    char letra = LETRA;

    if (letra >= 'a' && letra <= 'z')
        letra -= SALTO;

    else 
        letra += SALTO;

    printf ("letra en mayúsculas: %c\n", letra);

    return 0;


        }
