#include <stdio.h>
#include <stdlib.h>

enum TOpcion {triangulo, cuadrado};
const char *nombre[] = {"tri�ngulo", "cuadrado"};

double tri (double base, double alt) { return base * alt / 2; }
double cua (double base, double alt) { return base * alt    ; }
double error (double, double) {
    fprintf (stderr, "No s� qu� figura usar.\n");
    exit (1);
}

double (*area_disp[]) (double, double) = {&tri, &cua, &error};


void titulo () {
    system ("clear");
    system ("toilet -fpagga --gay AREA");
    printf ("\n\n");
}

enum TOpcion menu (){
    int opcion;

    titulo ();
    printf (
            "\t1.- Tri�ngulo\n"
            "\t2.- Cuadrado\n"
            "\n"
            "\tOpcion: ");
    scanf (" %i", &opcion);
    opcion--;

    return (enum TOpcion) opcion;
}

void preguntar_dim (double *base, double *alt) {
    printf ("Base: ");
    scanf (" %lf", base);
    printf ("Altura: ");
    scanf (" %lf", alt);
}

int main (int argc, char *argv[]) {

    double base, alt;
    double (*area)(double, double) = &error;

    enum TOpcion opcion = menu ();
    area = area_disp[opcion];

    preguntar_dim (&base, &alt);

    printf ("Area del %s = %.2lf\n", nombre[opcion], (*area)(base, alt));

    return EXIT_SUCCESS;
}

