#include <stdio.h>
#include <stdlib.h>

const char *name_number[3][10] = {
	{"", "primero", "segundo", "tercero", "cuarto", "quinto", "sexto",
     "séptimo", "octavo", "noveno"},
    {"", "décimo", "vigésimo", "trigésimo", "cuatrigésimo",
     "quincuagésimo", "sexagésimo", "septuagésimo", "octogésimo",
     "nonagésimo"},
	{"", "centésimo", "bicentésimo", "tricentésimo", "cuadrigentésimo",
     "quingentésimo", "sexgentésino", "septingentésimo",
     "octingentésimo", "noningentésimo"}
};

void ordinal (int n, int fila) {
	if (n != 0 ) {
		ordinal (n/10, fila++);
		printf ("%s ", name_number[fila][n % 10]);
	}
}

int main (int argc, char *argv[]){
	int number = 123;
	
	ordinal(number, 0);
}
