#include <stdio.h>
#include <stdlib.h>

const char *command = NULL;

void print_usage () {
    fprintf (stderr, "%s <posible primo>\n", command);
    exit (1);
}

bool divide (           /* Devuelve true si n_divisor o alg�n n�mero menor que �l divide a n_original*/
        int n_original, /* Numero original */
        int n_divisor   /* N�mero posible divisor */)
{
    if (n_divisor < 2)  /* Condici�n de salida. El 1 no interesa ahora para ver si un n�mero es primo. */
        return false;

    return  (n_original % n_divisor == 0) || divide (n_original, n_divisor - 1);
    /* Si el divisor divide al original no se hacen m�s c�lculos. De lo contrario hay que llamar a divide
     * para poder saber cu�l ser� el resultado del OR */
}

bool es_primo (int n) {
    return !divide (n, n/2);
}

int main (int argc, char *argv[]) {

    int n;

    command = argv[0]; //primer parametro introducido en la terminal

    if (argc < 2) //si tiene menos de dos parametros significa que el usuario no ha introducido el numero y devuelve el error
        print_usage ();

    n = atoi (argv[1]); /* atoi: ASCII to integer */ //despues de la instruccion de ejecucion el usuario debe meter el numero

    printf ("%i %s es primo.\n",
            n,
            es_primo (n) ? "s�" : "no"
            );

    printf ("\n");

    return EXIT_SUCCESS;
}

