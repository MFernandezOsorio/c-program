#include <stdio.h>
#include <stdlib.h>

void imprime (int n) {

    /* Condici�n de contorno */
    if (n<0)	//Si n es menor que 0 no devuelve nada
        return;

    printf ("%i ", n);
    imprime (n-1); //primero se imprime luego se resta
}

int main (int argc, char *argv[]) {

    const int n = 9;

    imprime (n); //Se llama a la funci�n

    printf ("\n");

    return EXIT_SUCCESS;
}

