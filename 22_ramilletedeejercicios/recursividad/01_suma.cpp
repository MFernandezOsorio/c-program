#include <stdio.h>
#include <stdlib.h>


int suma (int n){ //mientras que n sea menor o igual que 0 se hace la suma recursiva
    return n + (n <= 0 ? 0: suma (n-1) );

}

int main (int argc, char *argv[]) {

    const int n = 6;

    printf ("suma (%i) = %i\n", n, suma (n));

    return EXIT_SUCCESS;
}

