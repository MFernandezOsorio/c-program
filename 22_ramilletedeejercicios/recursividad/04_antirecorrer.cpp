#include <stdio.h>
#include <stdlib.h>

void imprime (int n) {

    /* Condici�n de contorno */
    if (n<0)
        return;

    imprime (n-1);
    printf ("%i ", n); //igual que el anterior pero poniendo el printf despu�s para que primero imprima n-1 y despues n
}

int main (int argc, char *argv[]) {

    const int n = 9;

    imprime (n);

    printf ("\n");

    return EXIT_SUCCESS;
}

