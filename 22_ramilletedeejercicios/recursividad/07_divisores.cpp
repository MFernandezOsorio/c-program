#include <stdio.h>
#include <stdlib.h>

void divisores (
        int n_original, /* Numero original */
        int n_divisor   /* N�mero posible divisor */)
{
    if (n_divisor < 1)  /* COndici�n de salida */
        return;

    if (n_original % n_divisor == 0) //para comprobar si es divisor el resto debe ser 0, si es 0 el resto se imprime por pantalla
        printf ("%i ", n_divisor);

    divisores (n_original, n_divisor - 1); //se le resta 1 a divisor para comprobar el siguiente numero si es divisor de n_original

}

int main (int argc, char *argv[]) {

    int n = 91;

    printf ("Divisores de %i => ", n);
    divisores (n, n/2);  // El divisor posible m�s grande de n es n/2

    printf ("\n");

    return EXIT_SUCCESS;
}

