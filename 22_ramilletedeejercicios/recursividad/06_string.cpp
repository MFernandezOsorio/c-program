#include <stdio.h>
#include <stdlib.h>

void imprime (const char *letra) {
    if (*letra != '\0') {
        imprime (letra + 1); //como el anterior pero se inprime al reves al cambiar el printf primero letra+1 y despues letra
        printf ("%c", *letra);
    }
}

int main (int argc, char *argv[]) {
    char frase[] = "dabale arroz a la zorra el abad";

    imprime (frase);

    printf ("\n");


    return EXIT_SUCCESS;
}

