#include <stdio.h>
#include <stdlib.h>

const char *command_name;

void printf_usage (FILE *output) { //salida si el usuario no introduce un numero despues de dar la instruccion en la terminal
    fprintf (output, "\
Usage:      %s <number>\n\
            This program converts a number into its ordinal counterpart.\n\
\n\
", command_name);

    exit (1);
}

const char *ordinal_name[3][10] = {
    {"", "primero", "segundo", "tercero", "cuarto", "quinto", "sexto",
     "séptimo", "octavo", "noveno"},
    {"", "décimo", "vigésimo", "trigésimo", "cuatrigésimo",
     "quincuagésimo", "sexagésimo", "septuagésimo", "octogésimo",
     "nonagésimo"},
    {"", "centésimo", "bicentésimo", "tricentésimo", "cuadrigentésimo",
     "quingentésimo", "sexgentésino", "septingentésimo",
     "octingentésimo", "noningentésimo"}
};

void ordinal (int n, int prof) {
    if ( n != 0 ) {
        ordinal (n / 10, prof+1);
        printf ("%s  ", ordinal_name[prof][n % 10]); //posicion de fila segun divisiones hechos y el resto para posicion del array de una fila
    }
}

int main(int argc, char *argv[])
{
    command_name = argv[0];

    if (argc < 2)
        printf_usage (stderr);  //si hay menos de 2 argumentos salida de error

    ordinal (atoi (argv[1]), 0);  //ascii to int

    printf ("\n");

    return EXIT_SUCCESS;
}

