#include <stdio.h>
#include <stdlib.h>

bool divide (           /* Devuelve true si n_divisor o alg�n n�mero menor que �l divide a n_original*/
        int n_original, /* Numero original */
        int n_divisor   /* N�mero posible divisor */)
{
    if (n_divisor < 2)  /* Condici�n de salida. El 1 no interesa ahora para ver si un n�mero es primo. */
        return false;

    return  (n_original % n_divisor == 0) || divide (n_original, n_divisor - 1); //Si no encuentra divisor que de resto 0 retornaria falso por la condicion del if
    /* Si el divisor divide al original no se hacen m�s c�lculos. De lo contrario hay que llamar a divide
     * para poder saber cu�l ser� el resultado del OR */
}

bool es_primo (int n) {
    return !divide (n, n/2);
}

int main (int argc, char *argv[]) {

    int n = 97;

    printf ("%i %s es primo.\n",
            n,
            es_primo (n) ? "s�" : "no"
            );

    printf ("\n");

    return EXIT_SUCCESS;
}

