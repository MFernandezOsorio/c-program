#include <stdio.h>
#include <stdlib.h>
/* jailbrake rebentar una cadena */

int main (int argc, char *argv[]) {
    char carta[] = "🂡";   // * va con char, tambien se puede poner
    //char carta[obligatorio poner cuantas celdas de memoria necesito, en este caso necesito 5]

    char *fin = carta + 3;
    ++*fin;
/*  Notacion de matrices
    carta[3]++;
*/
    ++(*(carta + 3));

    printf ("%s\n", carta);

return EXIT_SUCCESS;
}
