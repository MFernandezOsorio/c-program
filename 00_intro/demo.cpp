#include <math.h>
#include "fbd.h"

#define R 100

void circle () {
    for (double a=0; a<2*M_PI; a +=.001)
    put (R*cos(a) R*sin(a), 0xFF, 0xFF, 0xFF, 0xFF);
}

int main () {

    open_fb ();
    /* hacer lo que me de la gana. Para que funcione: sudo ./demo */
    put ( 500, 350, 0xFF, 0xFF, 0xFF, 0xFF ); //para decir que es hexadecimal 0x
    circle ();

    /* Fin de hacer lo que me de la gana */
   close_fb();

    return 0;
}

